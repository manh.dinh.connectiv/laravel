<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','aminControlleraminController@index')->name('login');

Route::get('/registration', function() {
    return view('registration');
});

Route::post('/login',['uses'=>'aminControlleraminController@dashboard'])->name('loginn');

Route::get('/dashbort', function() {
    return view('dashbort');
});
//dang ki

Route::post('/postRegistration',['uses'=>'registrationController@registration'])->name('registration');
Route::get('verifyAccount','registrationController@verifyAccount')->name('verifyAccount');
///


//sent mail

///lay lai mat khau
Route::get('/forgotpassword','ForgotPasswordorgotPassword@getFormResetPassword')->name('getFormResetPassword');

Route::post('/resetPassword', 'ForgotPasswordorgotPassword@resetPassword')->name('resetPassword');


Route::get('/veificatiCode', function() {
    return view('veification');
});
Route::post('/veification', 'ForgotPasswordorgotPassword@veification')->name('veification');

Route::get('/resetPassword/reset', 'ForgotPasswordorgotPassword@reset')->name('reset');

Route::post('/resetPassword/saveNewPassword', 'ForgotPasswordorgotPassword@saveNewPassword')->name('saveNewPassword');