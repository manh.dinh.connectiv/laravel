<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'       => 'required',
            'password_confirm' => 'required|same:password',
        ];
    }
    public function messges() {
        return [
            'password.required'     => 'trường này không được để trống',
            'password_comfirm'      => 'trường này không đươc để trống',
            'password_confirm'      => 'Mật khẩu xác nhận không đúng',
        ]
    }
}
