<?php

namespace App\Http\Controllers;

use \Carbon\Carbon;
use Illuminate\Http\Request;
use App\userAamin;
use Mail;
use Illuminate\Support\Facades\Redirect;


class registrationController extends Controller
{
    public function registration(Request $request) {
           
        $user = new userAamin();
        $user->name = $request->Name;
        $user->email = $request->Email;
        $checkUser = userAamin::where('email',$request->Email)->first();
        $user->password = MD5($request->Password);
        $user->active = '0';
        
        if($checkUser){
            return view('registration')->with('danger','Email đa được sử  dung');
        }else {
            $user->save();
            if($user->id) {
                
                $email = $user->email;
                $code = bcrypt(MD5(time().$email));
                $url = route('verifyAccount',['id'=>$user->id, 'code'=> $code]);
    
                $user->code_active = $code;
                $user->time_active = Carbon::now();
                $user->save();
    
               $data = ['route' => $url];
                Mail::send('email.veifAccount',$data, function($message) use($email){
                    $message->to($email, 'User')->subject('Veify account !');
                });
                return view('login')->with('alert','đang kí thành công bạn càn đăng nhập email để kích hoạt');
            }
        }
       

    }
    public function verifyAccount (Request $request) {
        $code = $request->code;
        $id = $request->id;
        $check = userAamin::where([
            'code_active' => $code,
            'id' => $id
        ])->first();
        if(!$check) {
            return view('login')->with('danger','xin loi! đường dẫn linh k chính xác');
        }
        $check->active = 1;
        $check->save();
       return view('registration')->with('messs','xac nhan thanh cong ');
    }

}
